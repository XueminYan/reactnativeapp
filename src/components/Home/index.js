import React from 'react';
import PropTypes from 'prop-types';
import { View, Text as NativeText } from 'react-native';
import { Image, Text, RichText } from '@sitecore-jss/sitecore-jss-react-native';
import styles, { richTextStyles } from './styles';

const Home = ({ fields, copyright, navigation }) => (
  <View style={styles.container}>
    <View style={styles.body}>
      <Image media={fields.imageLink} height="551" width="304" />
    </View>
  </View>
);

Home.propTypes = {
  imageLink: PropTypes.shape({
    value: PropTypes.shape({
      // This will be a number in disconnected mode (see dataService.disconnected.js), string in connected
      src: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
      alt: PropTypes.string,
    }),
  }),
};

Home.defaultProps = {
  copyright: 'Copyright Sitecore A/S test',
};

export default Home;
